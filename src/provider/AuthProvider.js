import React, { createContext, useState } from 'react';
import { authMethods } from '../firebase/authMethods';

export const firebaseAuth = createContext();

const initialInputsState = { email: '', password: '', username: '' };

const AuthProvider = (props) => {
  const [inputs, setInputs] = useState(initialInputsState);
  const [errors, setErrors] = useState('');
  const [token, setToken] = useState(null);
  const [user, setUser] = useState({ email: '', password: '', username: '' });
  const [loginStatus, setLoginStatus] = useState(false);

  const handleSignUp = () => {
    // middle man between firebase and signup
    authMethods.signup(
      inputs.email,
      inputs.password,
      setErrors,
      setToken,
      setLoginStatus
    );
    handleUserProfil();
    setInputs(initialInputsState);
  };
  console.log(errors);

  const handleSignIn = () => {
    // middle man between firebase and signup
    authMethods.signin(
      inputs.email,
      inputs.password,
      setErrors,
      setToken,
      setLoginStatus
    );
    setInputs(initialInputsState);
  };

  const handleSignOut = () => {
    authMethods.signout(setErrors, setToken, setLoginStatus);
  };

  const handleUserProfil = () => {
    authMethods.userProfil(inputs.username, setUser, setErrors);
  };

  return (
    <firebaseAuth.Provider
      value={{
        handleSignUp,
        handleSignIn,
        token,
        inputs,
        setInputs,
        errors,
        handleSignOut,
        loginStatus,
        setErrors,
        user
      }}
    >
      {props.children}
    </firebaseAuth.Provider>
  );
};

export default AuthProvider;
