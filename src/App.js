// import { useContext } from 'react';
// import { firebaseAuth } from './provider/AuthProvider';
import { Route, Switch } from 'react-router-dom';

import Navbar from './components/Navbar';
import SignUp from './components/SignUp';
import SignIn from './components/SignIn';
import Home from './components/Home';

function App() {
  return (
    <div className='bg-gray-50 h-full font-body'>
      <Navbar />
      <Switch>
        {/* route allows you to render by url path */}
        <Route exact path='/' component={Home} />
        {/* <Route
          exact
          path='/'
          render={() => (token === null ? <SignIn /> : <Home />)}
        /> */}
        <Route exact path='/signin' component={SignIn} />
        <Route exact path='/signup' component={SignUp} />
      </Switch>
    </div>
  );
}

export default App;
