import React, { useContext } from 'react';
import { firebaseAuth } from '../provider/AuthProvider';
import { withRouter } from 'react-router-dom';
// import { useSnackbar } from 'notistack';

import SignProcess from './SignProcess';

const SignUp = () => {
  const { handleSignUp } = useContext(firebaseAuth);

  return <SignProcess handleSignProcess={handleSignUp}>sign up</SignProcess>;
};

export default withRouter(SignUp);
