import React, { useContext, useEffect } from 'react';
import { firebaseAuth } from '../provider/AuthProvider';
import { withRouter } from 'react-router-dom';
import { useSnackbar } from 'notistack';

import InputForm from './form/InputForm';

const SignProcess = ({ handleSignProcess, history, children }) => {
  const {
    inputs,
    setInputs: setInputsContext,
    errors,
    loginStatus,
    setErrors: setErrorsContext
  } = useContext(firebaseAuth);

  const { enqueueSnackbar } = useSnackbar();
  const { email, username, password } = inputs;

  useEffect(() => {
    // On crée un effet qui se déclenche quand errors ou isLoggedIn change.
    if (errors !== '') {
      enqueueSnackbar(errors, {
        variant: 'error'
      });
      setErrorsContext('');
    } else if (loginStatus) {
      enqueueSnackbar('Welcome', {
        variant: 'success'
      });
      history.push('/');
    }
  }, [errors, loginStatus]);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleSignProcess();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputsContext((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className='container mx-auto flex justify-center py-9'>
      <form
        onSubmit={handleSubmit}
        className='flex justify-center flex-col w-1/2 max-w-xl p-20 rounded-lg shadow-md'
      >
        <h3 className='text-center text-xl pb-5'>{children}</h3>
        <InputForm name='email' value={email} handleChange={handleChange} />
        <InputForm
          name='password'
          value={password}
          handleChange={handleChange}
        />
        {children.toLowerCase() === 'sign up' && (
          <InputForm
            name='username'
            value={username}
            handleChange={handleChange}
          />
        )}
        <div className='flex justify-center'>
          <button className='py-2 px-3 w-28 bg-secondary-500 hover:bg-secondary-700 rounded-md text-white shadow-xl'>
            {children}
          </button>
        </div>
      </form>
      {/* <Link to='/'>HOME</Link> */}
    </div>
  );
};

export default withRouter(SignProcess);
