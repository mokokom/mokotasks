const InputForm = ({ handleChange, value, name }) => {
  return (
    <input
      className='mb-4 p-3 rounded shadow-md focus:outline-none focus:ring-2 focus:ring-primary-400 focus:border-transparent'
      onChange={handleChange}
      name={name}
      placeholder={name}
      value={value}
    />
  );
};

export default InputForm;
