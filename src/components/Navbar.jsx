import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { firebaseAuth } from '../provider/AuthProvider';

const Navbar = () => {
  const { loginStatus, handleSignOut, user } = useContext(firebaseAuth);
  let navMenu = (
    <>
      <NavLink
        to='/signin'
        className='p-2 w-18 mr-4 border-b-2 border-secondary-500 focus:outline-none hover:text-secondary-500'
      >
        sign in
      </NavLink>
      <NavLink
        to='/signup'
        className='p-2 w-18 mr-4 bg-secondary-500 hover:bg-secondary-700 rounded-md text-white shadow-xl focus:outline-none'
      >
        sign up
      </NavLink>
    </>
  );

  if (loginStatus)
    navMenu = (
      <>
        {/* <p>hello {user && user.username}</p> */}
        <button
          onClick={handleSignOut}
          className='p-2 w-18 mr-4 bg-secondary-500 hover:bg-secondary-700 rounded-md text-white shadow-xl focus:outline-none'
        >
          sign out
        </button>
      </>
    );

  return (
    <nav className='bg-primary-900 text-white h-16'>
      <div className='container mx-auto flex justify-between h-full items-center'>
        <div className='text-2xl'>
          <span className='text-secondary-900'>m</span>
          <span className='text-secondary-800'>o</span>
          <span className='text-secondary-700'>k</span>
          <span className='text-secondary-600'>o</span>
          <span className='text-secondary-500'>t</span>
          <span className='text-secondary-400'>a</span>
          <span className='text-secondary-300'>s</span>
          <span className='text-secondary-200'>k</span>
          <span className='text-secondary-100'>s</span>
          {/* <span className='text-yellow-500'>tasks</span> */}
        </div>
        <div className='flex'>{navMenu}</div>
      </div>
    </nav>
  );
};

export default Navbar;
