import React, { useContext } from 'react';
import { firebaseAuth } from '../provider/AuthProvider';
import { withRouter } from 'react-router-dom';
import SignProcess from './SignProcess';

const SignIn = (props) => {
  const { handleSignIn } = useContext(firebaseAuth);

  return (
    <>
      <SignProcess handleSignProcess={handleSignIn}>sign in</SignProcess>
    </>
  );
};

export default withRouter(SignIn);
