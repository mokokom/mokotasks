import firebaseconfig from './firebaseIndex';
import firebase from 'firebase';

export const authMethods = {
  signup: (email, password, setErrors, setToken, setLoginStatus) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      //make res asynchronous so that we can make grab the token before saving it.
      .then(async (res) => {
        const token = await Object.entries(res.user)[5][1].b;
        //set token to localStorage
        await localStorage.setItem('token', token);
        //grab token from local storage and set to state.
        setToken(window.localStorage.token);
        setLoginStatus(true);
      })
      .catch((err) => {
        setErrors(err.message);
      });
  },
  signin: (email, password, setErrors, setToken, setLoginStatus) => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      //make res asynchronous so that we can make grab the token before saving it.
      .then(async (res) => {
        const token = await Object.entries(res.user)[5][1].b;
        //set token to localStorage
        await localStorage.setItem('token', token);
        //grab token from local storage and set to state.
        setToken(window.localStorage.token);
        setLoginStatus(true);
      })
      .catch((err) => {
        setErrors(err.message);
      });
  },
  signout: (setErrors, setToken, setLoginStatus) => {
    // signOut is a no argument function
    firebase
      .auth()
      .signOut()
      .then(() => {
        //remove the token
        localStorage.removeItem('token');
        //set the token back to original state
        setToken(null);
        setLoginStatus(false);
      })
      .catch((err) => {
        //there shouldn't every be an error from firebase but just in case
        setErrors(err.message);
        //whether firebase does the trick or not i want my user to do there thing.
        localStorage.removeItem('token');
        setToken(null);
      });
  },
  userProfil: (username, setUser, setErrors) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        user
          .updateProfile({
            displayName: username,
            photoURL: 'https://example.com/jane-q-user/profile.jpg'
          })
          .then(() => {
            const updatedUser = {
              username: user.displayName,
              email: user.email
            };
            setUser(updatedUser);
          })
          .catch((err) => {
            setErrors(err.message);
          });
      }
    });
  }
};
