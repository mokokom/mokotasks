const colors = require('tailwindcss/colors');

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      body: ['Roboto']
    },
    extend: {
      colors: {
        primary: colors.gray,
        secondary: colors.blue
        // neutral: colors.gray
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
